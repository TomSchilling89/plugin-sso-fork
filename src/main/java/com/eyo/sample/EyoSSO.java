package com.eyo.sample;

import java.security.Key;

import org.jose4j.jwa.AlgorithmConstraints;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import com.google.inject.Inject;
import com.google.inject.name.Named;

public class EyoSSO {

  @Inject
  @Named(PluginModule.PLUGIN_ID)
  private String pluginID;

  @Inject
  @Named(PluginModule.PLUGIN_SECRET)
  private Key pluginSecretKey;

  /**
   * Decrypts/validates given eyo sso token.
   * @param encryptedSSO the eyo sso token.
   * @return The retrieved payload
   * @throws InvalidJwtException
   * @throws MalformedClaimException
   */
  public Data decrypt(final String encryptedSSO) throws InvalidJwtException, MalformedClaimException {

    final JwtConsumer c = new JwtConsumerBuilder()
        .setJwsAlgorithmConstraints(new
            AlgorithmConstraints(AlgorithmConstraints.ConstraintType.WHITELIST,
            AlgorithmIdentifiers.HMAC_SHA256))
        .setVerificationKey(pluginSecretKey)
        .setExpectedAudience(true, pluginID)
        .build();

    final JwtClaims jwtClaims = c.process(encryptedSSO).getJwtClaims();
    return new Data(
        jwtClaims.getClaimValue("eyo.i", String.class),
        jwtClaims.getClaimValue("eyo.u", String.class),
        jwtClaims.getClaimValue("eyo.ue", String.class),
        jwtClaims.getClaimValue("eyo.ufn", String.class),
        jwtClaims.getClaimValue("eyo.uln", String.class),
        jwtClaims.getClaimValue("eyo.ur", String.class),
        jwtClaims.getClaimValue("eyo.ul", String.class)
    );


  }

  /**
   * Holds the eyo sso payload.
   */
  public static class Data {
    public final String instanceID;
    public final String userID;
    public final String userExternalID;
    public final String userFirstName;
    public final String userLastName;
    public final String userRole;
    public final String userLocale;

    Data(
        final String instanceID,
        final String userID, final String userExternalID, final String userFirstName,
        final String userLastName, final String userRole, final String userLocale) {
      this.instanceID = instanceID;
      this.userID = userID;
      this.userExternalID = userExternalID;
      this.userFirstName = userFirstName;
      this.userLastName = userLastName;
      this.userRole = userRole;
      this.userLocale = userLocale;
    }

    @Override public String toString() {
      return String.format(
          "EyoSSO.Data{"
              + "instanceID='%s', userID='%s', userExternalID='%s', "
              + "userFirstName='%s', userLastName='%s', userRole='%s', userLocale='%s'}",
          instanceID, userID, userExternalID, userFirstName, userLastName, userRole, userLocale);
    }
  }
}
