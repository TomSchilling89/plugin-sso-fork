package com.eyo.sample;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;

import com.google.inject.Inject;

@Path("/")
public class PluginResource {

  private static final Logger logger = LogManager.getLogger(PluginResource.class);

  @Inject
  private EyoSSO eyoSSO;

  /**
   * EyoSSO Endpoint called by eyo backend.
   * The "eyosso" query param contains the jwt token.
   *
   * @param httpRequest
   * @param httpResponse
   * @param context
   * @param uriInfo
   * @return
   */
  @GET
  @Path("/eyosso")
  public Response eyosso(
      @Context final HttpServletRequest httpRequest,
      @Context final HttpServletResponse httpResponse,
      @Context final ServletContext context,
      @Context final UriInfo uriInfo) {

    final String encryptedSSO = uriInfo.getQueryParameters().getFirst("eyosso");

    if (logger.isDebugEnabled()) {
      logger.debug("sso started: {}",uriInfo.getRequestUri());
    }

    final EyoSSO.Data data;
    try {
      data = eyoSSO.decrypt(encryptedSSO);
    } catch (InvalidJwtException e) {
      logger.error("Unable to decode sso via jwt: {}", e.getMessage(), e);
      return forwardError(httpRequest, httpResponse, context);
    } catch (MalformedClaimException e) {
      logger.error("Unable to read claimed jwt value: {}", e.getMessage(), e);
      return forwardError(httpRequest, httpResponse, context);
    }

    if (logger.isDebugEnabled()) {
      logger.debug("sso successful: {}", data);
    }

    final RequestDispatcher dispatcher = context.getRequestDispatcher("/index.jsp");
    httpRequest.setAttribute("instanceID", data.instanceID);
    httpRequest.setAttribute("id", data.userID);
    httpRequest.setAttribute("externalID", data.userExternalID);
    httpRequest.setAttribute("firstName", data.userFirstName);
    httpRequest.setAttribute("lastName", data.userLastName);
    httpRequest.setAttribute("role", data.userRole);
    httpRequest.setAttribute("locale", data.userLocale);

    try {
      dispatcher.forward(httpRequest, httpResponse);
      return null;
    } catch (final Exception e) {
      logger.error("Unable to forward request to jsp: {}", e.getMessage(), e);
      return Response.status(500).build();
    }
  }

  private Response forwardError(
      final HttpServletRequest httpRequest,
      final HttpServletResponse httpResponse,
      final ServletContext context) {
    final RequestDispatcher dispatcher = context.getRequestDispatcher("/errors/500.jsp");
    try {
      dispatcher.forward(httpRequest, httpResponse);
      return null;
    } catch (final Exception e) {
      logger.error("Unable to forward request to error jsp: {}", e.getMessage(), e);
      return Response.status(500).build();
    }
  }
}
