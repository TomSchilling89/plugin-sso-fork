Eyo Plugin Skeleton (Java)
====================

This is a very basic eyo plugin.

## Usage ##
* set pluginID ```export eyo_plugin_id=my.plugin.id```
* set pluginSecret ```export eyo_plugin_secret=abcde12345...```
* build ```mvn clean package``` (generates war file in target folder)
* start ```mvn jetty:run```

## Overview ##

### EyoSSO ###
Decrypts to payload (user name, locale, ...) provided by the eyo backend.
The payload is transferred via jwt. To make decryption work, a valid pluginID (```eyo_plugin_id```) and
pluginSecret (```eyo_plugin_secret```) must be set via java opts or as environmental variables.

### PluginModule ###
Basic Guice Module.

### PluginResource ###
Jersey API Endpoint receiving the sso call from eyo backend.

### ServletContextListener ###
Sets up guice injection for the current servlet context.

### index.jsp ###
Example view showing the properties obtained during SSO.
